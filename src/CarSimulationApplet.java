import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class CarSimulationApplet extends JApplet implements ActionListener{
    private JTextField taskField;
    private JTextField lengthField;
    private JTextField startField;
    private JTextField middleField;
    private JPanel display;
    private HighwayApplet highway;
    private Thread taskThread;

    public void init(){
        setLayout(new BorderLayout());

        JPanel input = new JPanel();
        input.setLayout(new FlowLayout());

        input.add(new JLabel("Task Number"));
        taskField = new JTextField("1", 5);
        input.add(taskField);

        input.add(new JLabel("Highway Length"));
        lengthField = new JTextField("70",5);
        input.add(lengthField);

        input.add(new JLabel("Cars at the beginning"));
        startField = new JTextField("100", 5);
        input.add(startField);

        input.add(new JLabel("Cars at the interchange"));
        middleField = new JTextField(5);
        input.add(middleField);

        JButton runButton = new JButton("Run");
        runButton.addActionListener(this);
        input.add(runButton);

        add(input, BorderLayout.NORTH);

        display = new JPanel();
        add(display, BorderLayout.CENTER);
    }

    public void actionPerformed(ActionEvent e){
        int task = Integer.parseInt(taskField.getText());
        int length = Integer.parseInt(lengthField.getText());
        int start = Integer.parseInt(startField.getText());

        if(taskThread != null){
            if(taskThread.getName().equals("task1")){
                Task1Thread t = (Task1Thread)taskThread;
                t.terminate = 1;
            }
            else if(taskThread.getName().equals("task2")){
                Task2Thread t = (Task2Thread)taskThread;
                t.terminate = 1;
            }
            else{
                Task3Thread t = (Task3Thread)taskThread;
                t.terminate = 1;
            }
            while(taskThread.isAlive());
        }

        highway = new HighwayApplet(length, display);

        if(task == 1){
            taskThread = new Task1Thread(start, "task1");
            taskThread.start();
        }
        else if(task == 2){
            taskThread = new Task2Thread(start, "task2");
            taskThread.start();
        }
        else{
            int middle = Integer.parseInt(middleField.getText());
            taskThread = new Task3Thread(start, middle, "task3");
            taskThread.start();
        }
    }

    class Task1Thread extends Thread{
        private int start;
        private int terminate;

        public Task1Thread(int s, String name){
            super(name);
            start = s;
            terminate = 0;
        }
        public void run(){
            int firstPos, initSpeed;
            for(int i = 0;i < 200;i++){
                if(terminate == 1)
                    return;
                highway.print(); //print current status of highway
                if(start != 0){ //if their still cars waitting in the beginning
                    firstPos = highway.firstPos();
                    if(0 < (initSpeed = Math.min(4, (firstPos-1)/2))){ //if initial speed > 0
                        Car tmpCar = new Car(initSpeed);               //then set the speed and
                        highway.ary[0] = tmpCar;                       //put it to mark 1
                        start--;
                    }
                }
                highway.run(); //cars move at here
                try{
                    Thread.sleep(500);
                }
                catch(Exception e2){
                }
            }
        }
    }

    class Task2Thread extends Thread{
        private int start;
        private int terminate;

        public Task2Thread(int s, String name){
            super(name);
            start = s;
            terminate = 0;
        }
        public void run(){
            int firstPos, initSpeed, i;
            for(i = 0;i < 4;i++){
                if(terminate == 1)
                    return;
                highway.print();
                if(start != 0){
                    firstPos = highway.firstPos();
                    if(0 < (initSpeed = Math.min(4, (firstPos-1)/2))){
                        Car tmpCar = new Car(initSpeed);
                        highway.ary[0] = tmpCar;
                        start--;
                    }
                }
                highway.run();
                try{
                    Thread.sleep(500);
                }
                catch(Exception e2){
                }
            }

            // first driver gets headache at time 5
            if(terminate == 1)
                return;
            highway.print();
            highway.headache();
            if(start != 0){
                firstPos = highway.firstPos();
                if(0 < (initSpeed = Math.min(4, (firstPos-1)/2))){
                    Car tmpCar = new Car(initSpeed);
                    highway.ary[0] = tmpCar;
                    start--;
                }
            }
            highway.run();
            try{
                Thread.sleep(500);
            }
            catch(Exception e2){
            }
            i++;

            for(;i < 200;i++){
                if(terminate == 1)
                    return;
                highway.print();
                if(start != 0){
                    firstPos = highway.firstPos();
                    if(0 < (initSpeed = Math.min(4, (firstPos-1)/2))){
                        Car tmpCar = new Car(initSpeed);
                        highway.ary[0] = tmpCar;
                        start--;
                    }
                }
                highway.run();
                try{
                    Thread.sleep(500);
                }
                catch(Exception e2){
                }
            }
        }
    }

    class Task3Thread extends Thread{
        private int start;
        private int mid;
        private int terminate;

        public Task3Thread(int s, int m, String name){
            super(name);
            start = s;
            mid = m;
            terminate = 0;
        }
        public void run(){
            int firstPos, initSpeed, midPos;
            for(int i = 0;i < 200;i++){
                if(terminate == 1)
                    return;
                highway.print();
                if(start != 0){
                    firstPos = highway.firstPos();
                    if(0 < (initSpeed = Math.min(4, (firstPos-1)/2))){
                        Car tmpCar = new Car(initSpeed);
                        highway.ary[0] = tmpCar;
                        start--;
                    }
                }
                if(mid != 0){ //if their still cars waitting at the interchange
                    midPos = highway.midPos();
                    if(0 < (initSpeed = Math.min(4, (midPos-50)/2))){ //if initial speed > 0
                        Car tmpCar = new Car(initSpeed);              //then set the speed and
                        highway.ary[49] = tmpCar;                     //put it at mark 50
                        mid--;
                    }
                }
                highway.run();
                try{
                    Thread.sleep(500);
                }
                catch(Exception e2){
                }
            }
        }
    }
}
