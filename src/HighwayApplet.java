import javax.swing.*;
import java.awt.*;

public class HighwayApplet{
    /** It uses a static array to maintain cars' positions.*/
    public Car [] ary;
    /** highway's lengh*/
    public int len;

    private JPanel display;
    private JLabel [] labels;
    private ImageIcon car = new ImageIcon(getClass().getResource("car.gif"));

    /** print the current status of highway.*/
    public void print(){
        for(int i = 0;i < len;i++){
            if(ary[i] == null)
                labels[i].setVisible(false);
            else
                labels[i].setVisible(true);
        }
        display.repaint();
    }
    /** return first car's postion from beginning*/
    public int firstPos(){
        for(int i = 0;i < len;i++)
            if(ary[i] != null)
                return i+1;
        return 2147483647;
    }
    /** return first car's postion from mark 50*/
    public int midPos(){
        for(int i = 49;i < len;i++)
            if(ary[i] != null)
                return i+1;
        return 2147483647;
    }
    /**
     * Every time this method was called.
     * It move the car with respect to annouced speed strategy in one time unit.
     */
    public void run(){
        int front = 2147483647;
        for(int i = len-1;i >=0;i--){
            if(ary[i] != null){
                if(ary[i].nextSpeed != 0){
                    ary[i].curSpeed = ary[i].nextSpeed;
                    ary[i].nextSpeed = 0;
                }
                else if(ary[i].next2Speed != 0){
                    ary[i].nextSpeed = ary[i].next2Speed;
                    ary[i].next2Speed = 0;
                }
                else{
                    int tmpSpeed = Math.min(4, (front-i)/2);
                    if(tmpSpeed < ary[i].curSpeed)
                        ary[i].nextSpeed = tmpSpeed;
                    else if(tmpSpeed > ary[i].curSpeed)
                        ary[i].next2Speed = tmpSpeed;
                }
                if(i + ary[i].curSpeed < len)
                    ary[i+ary[i].curSpeed] = ary[i];
                ary[i] = null;
                front = i;
            }
        }
    }
    /** Let headache accident occurs in task 2*/
    public void headache(){
        int i = len-1;
        while(ary[i] == null)
            i--;
        ary[i].curSpeed /= 2;
        ary[i].nextSpeed = 0;
        ary[i].next2Speed = 0;
    }

    HighwayApplet(int l, JPanel d){
        len = l;
        display = d;
        ary = new Car[len];
        labels = new JLabel[len];
        display.removeAll();
        display.setLayout(new GridLayout(1, len));
        for(int i = 0;i < len;i++){
            labels[i] = new JLabel(car);
            labels[i].setVisible(false);
            display.add(labels[i]);
        }
        display.validate();
    }
}
