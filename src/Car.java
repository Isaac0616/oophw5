/**
 * This class is called Car. It is used to record the current speed
 * and the next, the next of next speed which it is going to change
 * for every car on highway
 *
 * @author b00902067
 */
public class Car{
    /** current speed*/
    public int curSpeed;
    /** speed should be at (t+1).0 implies no change.*/
    public int nextSpeed;
    /** speed should be at (t+2).0 implies no change.*/
    public int next2Speed;

    Car(int c){
        curSpeed = c;
    }
}
